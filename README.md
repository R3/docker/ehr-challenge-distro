# ehr-challenge-distro

EHR-Challenge-distro 


## Push a new docker image version
```
docker login git-r3lab.uni.lu:4567
docker build -t git-r3lab.uni.lu:4567/r3/docker/ehr-challenge-distro:1.01 . #replace 1.01 with whatever
docker build -t git-r3lab.uni.lu:4567/r3/docker/ehr-challenge-distro:latest .
docker push git-r3lab.uni.lu:4567/r3/docker/ehr-challenge-distro
```