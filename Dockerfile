FROM openjdk:8-jdk-slim
MAINTAINER Carlos Vega "carlos.vega@uni.lu"
# Install python
RUN apt-get update && \
        apt-get upgrade -y && \
        apt-get install -y python3.7 git python3-pip wget
COPY requirements.txt .
RUN pip3 install -r requirements.txt
RUN python3 -m pip install -r requirements.txt --default-timeout=180